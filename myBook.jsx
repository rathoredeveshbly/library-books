/** @format */

import React, { Component } from "react";
import axios from "axios";

class myBook extends Component {
  state = {
    posts: []
  };
  async componentDidMount() {
    const data = await axios.get("http://localhost:2410/booksapp/books");
    this.setState({ posts: data });
  }
  render() {
    console.log(this.state.posts.data);
    return (
      <div className="container">
        <div className="row">
          <div className="col-2">Left panel</div>
          <div className="col-10">
            <div className="row bg-primary">
              <div className="col-3 border">Title</div>
              <div className="col-2 border">Author</div>
              <div className="col-2 border">Language</div>
              <div className="col-2 border">Genre</div>
              <div className="col-2 border">Price</div>
              <div className="col-1 border">Bes...</div>
            </div>
            {this.state.posts.data.map(n1 => (
              <div className="row">
                <div className="col-3 border">Title</div>
                <div className="col-2 border">Author</div>
                <div className="col-2 border">Language</div>
                <div className="col-2 border">Genre</div>
                <div className="col-2 border">Price</div>
                <div className="col-1 border">Bes...</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default myBook;
